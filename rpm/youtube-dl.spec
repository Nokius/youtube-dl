Name:      youtube-dl
Version:   2018.02.11
Release:   0
Summary:   A tool for downloading video files from websites
License:   Unlicense
URL:       http://rg3.github.io/youtube-dl/
Source:    https://github.com/rg3/youtube-dl/releases/download/%version/%name-%version.tar.gz
BuildRequires:  zip
BuildRequires:  python
Requires:       python
BuildArch:      noarch

%description
youtube-dl is a small command-line program to retrieve videos from
YouTube.com and other video sites for later watching.

%prep
%setup -q -n %{name}-%{version}/%{name}

%build
make %{?_smp_mflags}

%install
make install

%clean
make clean

%files
/usr/local/bin/youtube-dl
/etc/bash_completion.d/youtube-dl
%defattr(-,root,root,-)
